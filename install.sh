#! /bin/bash

# Build Image
docker build --no-cache -t rms_registration:latest .

# Run Image
docker run -d --name rms_reg -v rms_reg_vol:/home/app -p 127.0.0.1:5000:8000 rms_registration:latest 

# Utils
# docker ps -aq # list
# docker stop $(docker ps -aq) # stop all
# docker rm $(docker ps -aq) # rm all cont.
# docker rmi $(docker images -q) # rm all images


# To Initalise Sqlite DB
# IMG_NAME=`docker inspect --format='{{.Name}}' $(docker ps -aq --no-trunc) | cut -c2-`

# echo $IMAGE_NAME
# docker exec -it rms_reg flask init-db

echo "Docker Container Setup successfully.\n The app is running in Port 5000"