import os
import functools
from flask import (
	Blueprint, flash, g, redirect, render_template, request, jsonify, url_for
)
from newsletter.db import get_db
from flask_mail import Mail,Message
from flask import current_app as app

bp = Blueprint('register', __name__, url_prefix='')


@bp.route('/register', methods=('GET', 'POST'))
def register():
	if request.method == 'POST':
		name = request.form['name']
		email = request.form['email']
		event = request.form['event']

		affiliation=request.form['affiliation']
		fsf_too=0
		verified=0
		
		if affiliation == '':
			affiliation = 'None'
		
		try:
			if(request.form['newsletter']=='on'):
				fsf_too=1
		except:
			pass
		

		db = get_db()
		error = None

		if not name:
			error = 'Name is required.'
		elif not email:
			error = 'Email is required.'
		elif db.execute(
			'SELECT email FROM users WHERE email = ?', (email,)
		).fetchone() is not None:
			error = 'User {} is already registered.'.format(email)

		if error is None:
			db.execute(
				'INSERT INTO users (email, name, event, affiliation, newsletter, verified, time) VALUES (?, ?, ?, ?, ?, ?, datetime())',
				(email, name, event, affiliation, fsf_too, verified)
			)
			db.commit()

			# Verification Mail Sending

			try:
				mail = Mail()
				mail.init_app(app)
				msg = Message('Confirm Your Email', sender = ('Free Software Foundation of India (FSF-India)', os.environ['mail_username']), recipients = [email])
				msg.body = 'Hello '+name+',\n\nThanks for Registering for the event at: '+event+'\n\n'+ \
				'Please visit the link given below to verify your email and complete your registration.:\n'+os.environ['SITE_URL']+'/verify/'+email
				msg.html = render_template('emails/confirm.html', name=name, email=email, event=event, site_url=os.environ['SITE_URL']) 
				status=mail.send(msg)
				print(status)
				# return str(status)
				return render_template('register/info.html')
			except Exception as e:
				# text="Invalid Email"
				text=str(e)
				return render_template('register/error.html', text=text, again=True)
			
			return render_template('register/info.html')

		# flash(error)
		return render_template('register/error.html', text=error, again=False)	
	else: 
		return redirect('https://rms-tour.gnu.org.in/registration/')


@bp.route('/verify/<email>')
def verify(email):
	db = get_db()
	
	row=db.execute(
		'SELECT verified,name,event,affiliation,newsletter FROM users WHERE email = ?', (email,)
	).fetchone()

	status=int(row[0])
	name=str(row[1])
	event=str(row[2])
	affiliation=str(row[3])
	fsf_too=int(row[4])
	
	if status:
		error = 'Your Email:{} is already verified.'.format(email)
		return render_template('register/error.html', text=error, again=False)
	else:
		try:
			# Update user table
			status = db.execute(
				'UPDATE users SET verified= ? WHERE email = ?', (1,email)
			).fetchone()
			db.commit()
			
			# For redundancy
			status = db.execute(
				'INSERT INTO verified (email, name, event, affiliation, newsletter, time) VALUES (?, ?, ?, ?, ?, datetime())', (email, name, event, affiliation,fsf_too)
			)
			db.commit()

			# Sending Email to Admin
			mail = Mail()
			mail.init_app(app)
			msg = Message('New Registration for: ' + event, sender = ('Free Software Foundation of India (FSF-India)', os.environ['mail_username']), recipients = [os.environ['admin_email']])
			msg.body = 'Hello Abhas,\n\nNew registration for the event at: '+event+'\n\n1. Full Name: '+name+'\n2. Email ID (verified): '+email+'\n3. Event: '+event+ \
			'\n4.Affiliation: '+affiliation+'\n5.FSF Newsletter Subscription: '+str(bool(fsf_too))
			msg.html = render_template('emails/new_reg.html', name=name, email=email, event=event, affiliation=affiliation, fsf_too=fsf_too) 
			status=mail.send(msg)
			print(status)
			return render_template('register/registered.html')
		except Exception as e:
			text=str(e)
			return render_template('register/error.html', text=text, again=True)

@bp.route('/users/<type>')
def users(type):
	db=get_db()
	if type=='all':
		rows=db.execute('SELECT * FROM users').fetchall()
		return jsonify([dict(ix) for ix in rows] )
	elif type=='verified':
		rows=db.execute('SELECT * FROM verified').fetchall()
		return jsonify([dict(ix) for ix in rows] )
	else:
		abort(404)
