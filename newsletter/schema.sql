DROP TABLE IF EXISTS users;
DROP TABLE IF EXISTS verified;

CREATE TABLE users (
  email TEXT PRIMARY KEY NOT NULL,
  name TEXT  NOT NULL,
  event TEXT NOT NULL,
  affiliation TEXT,
  newsletter INTEGER NOT NULL,
  time TEXT NOT NULL,
  verified INTEGER NOT NULL
);

CREATE TABLE verified (
  email TEXT PRIMARY KEY NOT NULL,
  name TEXT  NOT NULL,
  event TEXT NOT NULL,
  affiliation TEXT,
  newsletter INTEGER NOT NULL,
  time TEXT NOT NULL
);