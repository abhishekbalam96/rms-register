import os
from flask import Flask
from flask import render_template, send_from_directory
from flask_mail import Mail
import requests

class ReverseProxied(object):
	def __init__(self, app):
		self.app = app

	def __call__(self, environ, start_response):
		script_name = environ.get('HTTP_X_SCRIPT_NAME', '')
		if script_name:
			environ['SCRIPT_NAME'] = script_name
			path_info = environ['PATH_INFO']
			if path_info.startswith(script_name):
				environ['PATH_INFO'] = path_info[len(script_name):]

		scheme = environ.get('HTTP_X_SCHEME', '')
		if scheme:
			environ['wsgi.url_scheme'] = scheme
		
		server = environ.get('HTTP_X_FORWARDED_SERVER', '')
		if server:
			environ['HTTP_HOST'] = server
		return self.app(environ, start_response)

def get_events():
	url='https://rms-tour.gnu.org.in/events.json'
	try:
		events=requests.get(url).json()
		print(events)
		return events
	except:
		return {}
		
def create_app(test_config=None):
	app = Flask(__name__, instance_relative_config=True)
	app.wsgi_app = ReverseProxied(app.wsgi_app)
	app.config.from_mapping(
		SECRET_KEY=os.environ['SECRET_KEY'],
		DATABASE=os.path.join(app.instance_path, 'registrations.sqlite3'),
		MAIL_SERVER=os.environ['mail_server'],
		MAIL_PORT=25,
		# MAIL_USERNAME=os.environ['mail_username'],
		# MAIL_PASSWORD=os.environ['mail_password'],
		MAIL_USE_TLS=True,
		MAIL_USE_SSL=False,
		APP_TOKEN=os.environ['APP_TOKEN']
	)

	try:
		os.makedirs(app.instance_path)
	except OSError:
		pass

	from . import db
	db.init_app(app)

	from . import register
	app.register_blueprint(register.bp)

	@app.route('/')
	def main():
		events=get_events()
		if events:
			return render_template('register/index.html', site_url=os.environ['SITE_URL'], events=events)
		else:
			return "No events.json at root!"
			
	return app