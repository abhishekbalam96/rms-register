from setuptools import find_packages, setup

setup(
	name='newsletter',
	version='1.0.0',
	url='https://www.djangoproject.com/',
	author='Django Software Foundation',
	author_email='foundation@djangoproject.com',
	description=('A high-level Python Web framework that encourages '
				 'rapid development and clean, pragmatic design.'),
	license='BSD',
	packages=find_packages(),
	include_package_data=True,
	zip_safe=False,
	install_requires=[
		'flask',
	],
)