FROM python:2.7-alpine

RUN mkdir -p /home/app

COPY dev.sh /home/app
COPY requirements.txt /home/app
COPY setup.py /home/app
COPY MANIFEST.in /home/app

RUN mkdir -p /home/app/newsletter
ADD newsletter /home/app/newsletter

WORKDIR /home/app

RUN pip install -r requirements.txt

ENV FLASK_APP=newsletter
ENV FLASK_ENV=production
ENV APP_TOKEN=fumero 
ENV SECRET_KEY=dev
ENV SITE_URL="https://rms-tour.gnu.org.in/registration"
ENV GUNICORN_WORKERS=4
ENV GUNICORN_BIND=0.0.0.0:8000

ENV mail_password=sPYqtFN7xx
ENV mail_username=no-reply@events.gnu.org.in
ENV mail_server=abra.hammer.today
# ENV admin_email=abhas@deeproot.in
ENV admin_email=abhas@deeproot.in

EXPOSE 8000

RUN flask init-db

ENTRYPOINT ["gunicorn"]
CMD ["-w 4","-b","0.0.0.0:8000", "newsletter.wsgi:app"]